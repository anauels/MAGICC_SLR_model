# MAGICC SEA LEVEL MODEL

This is the latest version of the MAGICC sea level model, status: 23 March 2017.

The Fortran code of the model as well as required additional MAGICC input files can be downloaded as a zip-file above.

Please note that the MAGICC sea level model is not a stand-alone tool. It is fully integrated into the simple carbon-cycle climate model MAGICC. The MAGICC model is described in Meinshausen et al. (2011). 

For a compiled or source code version of MAGICC including the sea level model, please contact `alexander.nauels@climate-enegy-college.org`.

Written by (c) Alexander Nauels, March 2017.

## MODULE STRUCTURE & FUNCTIONALITY

The sea level model Fortran code can be found in the `src` directory. Within MAGICC, the model is integrated as the subroutine `CALC_SEALEVEL`. The main sea level parameters are defined in the MAGICC namelist (see below). Additional input is read from the file `SLR_DEFAULT_PARAMS.IN`. This file together with calibration parameter sets for the relevant sea level components are provided in the `run` directory. Specific reference model parameters are called with the MAGICC namelist entries `FILE_TUNINGMODEL_SLR_XX`.

MAGICC namelist entries relevant to the sea level model:
```
...
e.g.
FILE_TUNINGMODEL_1="OCNTUNE_CESM1CAM5",
FILE_TUNINGMODEL_2="GLACIERTUNE_CCSM4",
FILE_TUNINGMODEL_3="GISSMBTUNE_CMCCCM",
FILE_TUNINGMODEL_4="AISSIDTUNE_MRICGCM3",
FILE_TUNINGMODEL_5="AISSMBTUNE_HADCM3",
...
SLR_EXPANSION_SCALING =  8.823828e-01,  
SLR_GL_SENS_MMPYRDEG =  6.250000e-01,  
SLR_GL_TEMP_EXPONENT =  8.200000e-01,  
SLR_GL_NORMPARA_TEMP =  1,  
SLR_GL_NORMPARA_VOL =  1,  
SLR_GIS_SMB_PARAMETERISATION =  "DEFAULT",  
SLR_GIS_SMB_STARTYEAR =  1850,  
SLR_GIS_SMB_COEF_FW1 =  -7.150000e+01,  
SLR_GIS_SMB_COEF_FW2 =  2.040000e+01,  
SLR_GIS_SMB_COEF_FW3 =  2.800000e+00,  
SLR_GIS_SMB_COEF1 =  1.500000e-02,  
SLR_GIS_SMB_COEF2 =  9.900000e-01,  
SLR_GIS_SMB_SENS_EXPONENT =  2.300000e+00,  
SLR_GIS_SMB_INITIAL_VOLUME_MM =  7380,  
SLR_GIS_SMB_VOLUME_EXPONENT =  5.000000e-01,  
SLR_GIS_SID_STARTYEAR =  1850,  
SLR_GIS_SID_CASE =  5.000000e-01,  
SLR_GIS_SID_DSCHRG_SENS_LOW =  9.061700e-04,  
SLR_GIS_SID_DSCHRG_SENS_HIGH =  7.933000e-04,  
SLR_GIS_SID_TOTALVOL_LOW =  3.598030e+01,  
SLR_GIS_SID_TOTALVOL_HIGH =  5.362890e+01,  
SLR_GIS_SID_TEMPSENS_EXPONENT_LOW =  3.891900e-01,  
SLR_GIS_SID_TEMPSENS_EXPONENT_HIGH =  4.721600e-01,  
SLR_GIS_SID_SCALING =  5,  
SLR_AIS_SMB_STARTYEAR =  1850,  
SLR_AIS_SMB_COEF1 =  1.279988e-01,  
SLR_AIS_SMB_COEF2 =  -4.243880e-01,  
SLR_AIS_SMB_SENS_EXPONENT =  7.819579e-01,  
SLR_AIS_SID_STARTYEAR =  1850,  
SLR_AIS_SID_ICEMODEL =  "PENNSTATE3D",  
SLR_AIS_SID_PRESCRIBE_BASALMELT =  1,  
SLR_AIS_SID_BASALMELT =  1.150000e+01,  
SLR_AIS_SID_BASALMELT_SENS =  7,  16,  
SLR_AIS_SID_DT_AMUNDSEN =  0,  
SLR_AIS_SID_TEMPSCALING_AMUNDSEN =  1.700000e-01,  
SLR_AIS_SID_DT_EASTANTARCTICA =  30,  
SLR_AIS_SID_TEMPSCALING_EASTANTARCTICA =  3.500000e-01,  
SLR_AIS_SID_DT_ROSS =  20,  
SLR_AIS_SID_TEMPSCALING_ROSS =  2.600000e-01,  
SLR_AIS_SID_DT_WEDDELL =  35,  
SLR_AIS_SID_TEMPSCALING_WEDDELL =  1.400000e-01,  
SLR_LANDWATER_SWITCH =  1,  
SLR_LANDWATER_CASE =  5.000000e-01,  
SLR_LANDWATER_STARTYEAR =  1900,  
SLR_LANDWATER_SWITCHYEAR =  2100,  
SLR_LANDWATER_MAXVOLUME_MM =  1000,  
SLR_LANDWATER_VOLUME_EXPONENT =  5.000000e-01,  
FILE_SLR_DEFAULT_PARAMS =  "SLR_DEFAULT_PARAMS.IN",
...
```

## MAGICC SEA LEVEL MODEL LICENSE

This code is licensed under GPLv3, please see the LICENSE.txt authored by `alexander.nauels@climate-energy-college.org`.

## MAGICC PARENT MODEL LICENSES

The MAGICC executable is provided with a [Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/) license. The MAGICC source code is available under a separate license agreement. Any derivatives have to be fed back to the MAGICC developers, so that users of future MAGICC versions can have the benefit of applying the model alterations, enhancements etc. Furthermore, we would like you to provide feedback, bug reports and development suggestions.

## REFERENCE

Meinshausen, M., Raper, S. C. B., and Wigley, T. M. L.: Emulating coupled atmosphere-ocean and carbon cycle models with a simpler model, MAGICC6 - Part 1: Model description and calibration, Atmospheric Chemistry and Physics, 11, 1417–1456, 2011.